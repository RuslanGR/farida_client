import React from "react";
import { NavLink } from "react-router-dom";
import "./index.css";

const Navigation = () => (
  <nav className="navbar navbar-expand-lg navbar-light pb-4">
    <div className="container">
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink activeClassName="active-item" className="nav-link" exact to="/">
              Главная
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink activeClassName="active-item" className="nav-link" to="/doctors">
              Врачи
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink activeClassName="active-item" className="nav-link" to="/about">
              О нас
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

export default Navigation;
