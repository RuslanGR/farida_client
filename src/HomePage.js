import React from "react"
import {Link} from 'react-router-dom'

class HomePage extends React.Component {
  state = {};

  render() {
    return (
      <div className="container">
          <h1>Проверка корректности работы сотрудников больницы</h1>
          <p>Здесь вы можете <Link to="/doctors">посмотреть отзывы</Link> или просто найти интересующую вас информаицю</p>
      </div>
    )
  }
}

export default HomePage;
