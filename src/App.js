import React from 'react';
import {Route} from "react-router";
import AboutPage from "./AboutPage";
import DoctorsPage from "./DoctorsPage";
import DoctorPage from "./DoctorPage";
import HomePage from "./HomePage";
import Navigation from "./Navigation";

function App() {
  return (
    <div className="App">
      <Navigation/>
      <div>
        <Route path="/" exact component={HomePage} />
        <Route path="/about" exact component={AboutPage} />
        <Route path="/doctors/:id" component={DoctorPage} />
        <Route path="/doctors" exact component={DoctorsPage} />
      </div>
    </div>
  );
}

export default App;
