import React from "react";
import img from "./assets/hospital.jpg";

class PatientsPage extends React.Component {
  state = {};

  render() {
    return (
      <div className="container">
        <h1>О нас</h1>
        <p>Адрес: Город Казань, кремлевская 35</p>
        <p>Телефон горячей линии: +78005553535</p>

        <img src={img} alt="hospital"/>
      </div>
    );
  }
}

export default PatientsPage;
