import React from "react";
import axios from "axios";

class DoctorPage extends React.Component {
  state = {
    doctor: {},
    comments: []
  };

  componentDidMount() {
    const {
      params: { id }
    } = this.props.match;
    axios
      .get(`http://127.0.0.1:8000/api/doctors/${id}/`)
      .then(res =>
        this.setState({ doctor: res.data }, () => console.log(this.state))
      );
    axios
      .get(`http://127.0.0.1:8000/api/comments/${id}/`)
      .then(res => this.setState({ comments: res.data.comments }));
  }

  render() {
    const { doctor, comments } = this.state;
    return (
      <div className="container">
        {doctor && doctor.user && (
          <div>
            <h1>Доктор {doctor.user.username}</h1>
            <h4>Телефон {doctor.phone_number}</h4>
          </div>
        )}
          <h5 className="pt-4">Комментарии о работе доктора</h5>
        {comments &&
          comments.map(comment => (
            <div className="border py-1 my-1 rounded px-2">
              {comment.sender && comment.sender.user && (
                <p>{comment.sender.user.username}: {comment.text}</p>
              )}
            </div>
          ))}
      </div>
    );
  }
}

export default DoctorPage;
