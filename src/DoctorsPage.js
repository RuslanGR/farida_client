import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Card = ({ doctor }) => {
  return (
    <div className="card my-2">
      <div className="card-header">Доктор</div>
      <div className="card-body">
        <h5 className="card-title">Имя: {doctor.user.username}</h5>
        <p className="card-text">Номер телефона: {doctor.phone_number}</p>
        <Link to={`/doctors/${doctor.id}`} className="btn btn-secondary">
          Посмотреть профиль
        </Link>
      </div>
    </div>
  );
};

class DoctorsPage extends React.Component {
  state = {
    doctors: []
  };

  componentDidMount() {
    axios
      .get("http://127.0.0.1:8000/api/doctors/")
      .then(res =>
        this.setState({ doctors: res.data.results }, () =>
          console.log(this.state)
        )
      );
  }

  render() {
    const { doctors } = this.state;
    return (
      <div className="container">
        <h1>Доктора в нашем отделении</h1>
        <div>
          {doctors.length ? (
            doctors.map(doctor => <Card doctor={doctor} />)
          ) : (
            <p>Загрузка</p>
          )}
        </div>
      </div>
    );
  }
}

export default DoctorsPage;
